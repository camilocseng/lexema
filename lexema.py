#!/usr/bin/env python3

import re

def lexemas(text):
    """
    Split the text into words and punctuation marks
    excluding spaces.

    - text : string with the text.
    - return : generator of strings with the words.

    Note that texts or languages with
    neither spaces nor punctuation marks
    will not work correctly.
    """
    return (match.group() for match in re.finditer(r"\w+|[^ \t\r\n]", text))

def lexemas1(text):
    """
    Similar to its predecessor but including spaces.
    """
    return (match.group() for match in re.finditer(r"(?s)\w+|.", text))

def main():
    import sys

    for line in sys.stdin:
        for lexema in lexemas(line):
            print(lexema)

if __name__ == "__main__":
    main()
